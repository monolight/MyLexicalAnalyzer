import java.util.*;
import java.util.regex.Pattern;

public class AnalyzeTool implements Runnable {
    private static Vector<String> keywordlist;
    private String content;

    public AnalyzeTool(String content) {
        this.content = content;
        keywordlist = new Vector<String>();
        //添加关键字列表
        for (String temp : Keyword.keywords) {
            keywordlist.add(temp);
        }
        //添加符号列表
        for (String temp : Sign.signs) {
            keywordlist.add(temp);
        }
    }

    public int countSpecificSign(String str, char sign) {
        int count = 0;
        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == sign)
                count++;
        }
        return count;
    }

    public boolean isInteger(String str) {      //判断是否为整数
        Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
        return pattern.matcher(str).matches();
    }

    public boolean isConsChar(String str) {       //判断是否为字符常数
        if (str.startsWith("'")
            && str.endsWith("'")
            && str.length() > 1
            && countSpecificSign(str, '\'') == 2)
            return true;
        return false;
    }

    public boolean isMarker(String str) {
        //<标识符> → <字母>│_ <字母>│_ <数字>│<标识符> <数字>│<标识符> <字母>
        Pattern pattern = Pattern.compile("^[a-zA-Z]*$|^_[a-zA-Z\\d]*$");       //待验证
        return pattern.matcher(str).matches();
    }

    public Vector<String> dispart() {         //拆解单词
        //处理错误
        //1、处理非法字符
        int line = 1;
        for (int i = 0; i < content.length(); i++) {
            char curchar = content.charAt(i);
            //处理换行
            if (i < content.length() - 1 && content.substring(i, i+2).equals("\r\n")) {
                line++;
                i++;
                continue;
            }
            if (curchar == '\r' || curchar == '\n') {
                line++;
                continue;
            }
            //处理空格
            if (curchar == ' ' || curchar == '\t')
                continue;
            //处理关键字
            if (Keyword.isKeyword(String.valueOf(curchar)))
                continue;
            //处理符号
            if (Sign.isSign(String.valueOf(curchar)))
                continue;
            //处理字母
            if (String.valueOf(curchar).matches("[a-zA-Z]"))
                continue;
            //处理数字
            if (String.valueOf(curchar).matches("[0-9]"))
                continue;
            //处理单引号
            if (curchar == '\'')
                continue;
            System.out.println("非法字符！"+String.valueOf(curchar)+"在第"+line+"行");
        }
        //2、处理单引号配对问题
        line = 1;
        int lastline = 1;
        boolean stack = false;
        for (int i = 0; i < content.length(); i++) {
            char curchar = content.charAt(i);
            //处理换行
            if (i < content.length() - 1 && content.substring(i, i+2).equals("\r\n")) {
                line++;
                i++;
                continue;
            }
            if (curchar == '\r' || curchar == '\n') {
                line++;
                continue;
            }
            //单引号判断
            if (stack && line != lastline) {     //缺少单引号
                System.out.println("缺少单引号！在"+lastline+"行");
                stack = false;
            }
            if (stack && curchar == '\'') {
                stack = false;
                continue;
            }
            if (!stack && curchar == '\'') {
                lastline = line;
                stack = true;
                continue;
            }
        }
        //3、处理注释配对问题
        line = 1;
        lastline = 1;
        stack = false;
        for (int i = 0; i < content.length() - 1; i++) {
            char firstchar = content.charAt(i);
            char secondchar = content.charAt(i+1);

            //处理换行
            if (i < content.length() - 1 && content.substring(i, i+2).equals("\r\n")) {
                line++;
                i++;
                continue;
            }
            if (firstchar == '\r' || firstchar == '\n') {
                line++;
                continue;
            }
            //注释判断
            if (stack && line != lastline) {     //缺少右注释
                System.out.println("缺少右注释！在"+lastline+"行");
                stack = false;
            }
            if (stack && firstchar == '*' && secondchar == '/') {
                stack = false;
                continue;
            }
            if (!stack && firstchar == '/' && secondchar == '*') {
                lastline = line;
                stack = true;
                continue;
            }
        }

        //消除注释
        content = content.replaceAll("/\\*(.*?)\\*/", "");

        Vector<String> words = new Vector<String>();
        int p = 0, q = 0;
        boolean flag = false;
        for (int i = 0; i < content.length(); i++) {
            //System.out.println(content.charAt(i));
            //处理符号
            
            //1、处理带两个字符的符号
            if (i < content.length() - 1) {
                if (!flag && Sign.isSign(content.substring(i, i+2))) {
                    q = i;
                    words.add(content.substring(p, q));
                    p = i;
                    q = i+2;
                    words.add(content.substring(p, q));
                    p = i+2;
                    i++;        //加上循环里的i++，其实是i+=2
                    flag = true;
                    continue;
                }
            }
            //2、处理带一个字符的符号
            if (i < content.length()) {
                if (!flag && Sign.isSign(content.substring(i, i+1))) {
                    q = i;
                    words.add(content.substring(p, q));
                    p = i;
                    q = i+1;
                    words.add(content.substring(p, q));
                    p = i+1;
                    flag = true;
                    continue;
                }
            }


            //找到单词结束位置
            if (!flag && String.valueOf(content.charAt(i)).matches("\\s")) {
                q = i;
                flag = true;
                words.add(content.substring(p, q));
                continue;
            } else if (!flag && i == content.length() - 1) {
                q = i;
                words.add(content.substring(p));
                continue;
            }
            //找到下一单词开始位置
            if (flag && !String.valueOf(content.charAt(i)).matches("\\s")) {
                p = i;
                flag = false;
                continue;
            }
        }
        return words;
    }

    public Vector<Result> analyze(Vector<String> words) {
        Vector<Result> result = new Vector<Result>();
        for (String temp : words) {
            if (Keyword.isKeyword(temp)) {      //判断关键字
                Result res = new Result(Keyword.getCode(temp), temp);
                result.add(res);
                continue;
            } else if (Sign.isSign(temp)) {            //判断符号
                Result res = new Result(Sign.getCode(temp), temp);
                result.add(res);
                continue;
            } else if (isConsChar(temp)) {             //判断字符常数
                Result res = new Result(36, temp);
                result.add(res);
                continue;
            } else if (isInteger(temp)) {           //判断整数
                Result res = new Result(35, temp);
                result.add(res);
                continue;
            } else if (isMarker(temp)) {
                Result res = new Result(34, temp);
                result.add(res);
                continue;
            } else {
                continue;
            }
        }
        return result;
    }

    public void run() {
        Vector<String> words = this.dispart();      //拆解
        Vector<Result> result = this.analyze(words);        //分析
        for (Result temp : result) {
            System.out.println(temp);
        }
    }
}