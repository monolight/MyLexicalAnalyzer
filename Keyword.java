public class Keyword {
    public static String[] keywords = {
        "and",
        "array",
        "begin",
        "bool",
        "call",
        "case",
        "char",
        "const",
        "do",
        "double",
        "else",
        "end",
        "false",
        "for",
        "if",
        "int",
        "not",
        "of",
        "or",
        "procedure",
        "program",
        "read",
        "real",
        "repeat",
        "set",
        "stop",
        "then",
        "to",
        "true",
        "until",
        "var",
        "while",
        "write"
    };
    public static boolean isKeyword(String target) {
        for (String temp : keywords) {
            if (temp.equals(target))
                return true;
        }
        return false;
    }
    public static int getCode(String target) {
        for(int i=0; i<keywords.length; i++) {
            if(target.equals(keywords[i]))
                return i+1;
        }
        return -1;
    }
}