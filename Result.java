public class Result{
    private int typeCode;
    private String word;
    public Result(){
        this(0,null);
    }
    public Result(int typeCode,String word){
        this.typeCode=typeCode;
        this.word=word;
    }
    public int getTypeCode(){
        return typeCode;
    }  
    public String getWord(){
        return word;
    }
    public void setTypeCode(int typeCode){
            this.typeCode=typeCode;         
    }
    public void setWord(String word){
        this.word=word;
    }
    public String toString(){
        return "("+typeCode+","+word+")";
    }

}