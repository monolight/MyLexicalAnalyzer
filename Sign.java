public class Sign {
    public static String[] signs = {
        "(",
        ")",
        "*",
        "*/",
        "+",
        ",",
        "-",
        ".",
        "/",
        "/*",
        ":",
        ":=",
        ";",
        "<",
        "<=",
        "=",
        ">",
        ">=",
        "[",
        "]",
        "_",
        "{",
        "}",
        "%",
    };
    public static boolean isSign(String target) {
        for (String temp : signs) {
            if (temp.equals(target))
                return true;
        }
        return false;
    }
    public static int getCode(String target) {
        for(int i=0; i<signs.length; i++) {
            if(target.equals(signs[i]))
                return i+37;
        }
        return -1;
    }
}